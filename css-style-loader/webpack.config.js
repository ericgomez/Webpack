const path = require('path') /*este es un modulo de node y se encarga de la gestion de rutas*/

/*Para exportar un modulo, se debe hacer de la forma de commonJS*/
module.exports = {
    entry: {/*Almacenar multiples directiorios como un object */
        home: path.resolve(__dirname, 'src', 'js', 'index'),
    }, 
    mode: 'development', /* modo de ejecución */
    output:{ /*Configuraciones para el archivo final que generará webpack */
        path: path.resolve(__dirname, 'dist'), /* La ruta en donde quedará el archivo final */
        filename: 'js/[name].js' /*[name] es un template que corresponde al nombre del contenido de los entry - El nombre sera diferente al momento de crear el archivo final */
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    } 
}