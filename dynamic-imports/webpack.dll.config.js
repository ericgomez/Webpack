const path = require('path') /*este es un modulo de node y se encarga de la gestion de rutas*/
const webpack = require('webpack')

/*Para exportar un modulo, se debe hacer de la forma de commonJS*/
module.exports = {
    entry: {/*Almacenar multiples directiorios como un object */
        modules: [
            'react',
            'react-dom'
        ]
    }, 
    mode: 'production', /* modo de ejecución */
    output:{ /*Configuraciones para el archivo final que generará webpack */
        path:path.resolve(__dirname, "dist"), /* La ruta en donde quedará el archivo final */
        filename: "js/[name].js", /*[name] es un template que corresponde al nombre del contenido de los entry - El nombre sera diferente al momento de crear el archivo final */
        library: '[name]', /* Nombre de la variable gobal que estara como libreria */
    },
    plugins: [
        new webpack.DllPlugin({
            name: "[name]",//nombre de mi archivo DLL
            path: path.join(__dirname, "[name]-manifest.json")//donde lo quiero ubicar
        })
    ],
}