const path = require('path')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    entry: {
        home: path.resolve(__dirname,'src/js/index.js'),
        contact: path.resolve(__dirname,'src/js/contact.js'),
    },
    mode: 'production',
    output:{
        path: path.resolve(__dirname, 'dist'), 
        filename:  'js/[name].js'
    },
    module:{
        rules: [ 
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: [
                    /*reemplazamos el style-loader por el MiniCSSExtractPlugin ya que en produccion
                    no necesitamos el css inyectado sino el archivo de css final*/
                    {
                        loader: MiniCSSExtractPlugin.loader,
                        options: {
                          publicPath: ''
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    'postcss-loader',
                ]   
            },
            {
                test: /\.less$/,
                use: [
                    /*reemplazamos el style-loader por el MiniCSSExtractPlugin ya que en produccion
                    no necesitamos el css inyectado sino el archivo de css final*/
                    {
                        loader: MiniCSSExtractPlugin.loader,
                    },
                    'css-loader',
                    'less-loader',
                ]   
            },
            {
                test: /\.scss$/,
                use: [
                    /*reemplazamos el style-loader por el MiniCSSExtractPlugin ya que en produccion
                    no necesitamos el css inyectado sino el archivo de css final*/
                    {
                        loader: MiniCSSExtractPlugin.loader,
                    },
                    'css-loader',
                    'sass-loader',
                ]   
            },
            {
                test: /\.styl$/,
                use: [
                    /*reemplazamos el style-loader por el MiniCSSExtractPlugin ya que en produccion
                    no necesitamos el css inyectado sino el archivo de css final*/
                    {
                        loader: MiniCSSExtractPlugin.loader,
                    },
                    'css-loader',
                    'stylus-loader',
                ]   
            },

            {
                test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 90000, 

                    }
                }
            }
        ]
    },
    plugins: [
        /*Agregamos este plugin para generar los archivos de css*/
        new MiniCSSExtractPlugin({
            /*Le damos el nombre de archivo que va a generar*/
            filename: 'css/[name].css',
            /*Como queremos partir nuestro archivos de manera optimizada cuando se vayan a repetir
            y el nombre sera generando un id */
            chunkFilename: 'css/[id].css'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'webpack-dev-server',
            template: path.resolve(__dirname,'index.html'),
        }),
        /*Aqui necesitamos un plugin para consumir el dll, osea que este que es el webpack principal
        recibira el webpack.dll*/
        new webpack.DllReferencePlugin({
            manifest: require('./modules-manifest.json')
        })
        /*Aqui pondremos el key principal en donde le daremos la ruta del manifest que exportamos en 
        el webpack.dll*/ 
    ],
}