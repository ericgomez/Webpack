const path = require('path') /*este es un modulo de node y se encarga de la gestion de rutas*/
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const HtmlWepackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

/*Para exportar un modulo, se debe hacer de la forma de commonJS*/
module.exports = {
    entry: {/*Almacenar multiples directiorios como un object */
        home: path.resolve(__dirname, 'src', 'js', 'index'),
        contact: path.resolve(__dirname, 'src', 'js', 'contact'),
    }, 
    mode: 'production', /* modo de ejecución */
    output:{ /*Configuraciones para el archivo final que generará webpack */
        path:path.resolve(__dirname, "dist"), /* La ruta en donde quedará el archivo final */
        filename: "js/[name].js" /*[name] es un template que corresponde al nombre del contenido de los entry - El nombre sera diferente al momento de crear el archivo final */
    },
     devServer: {
         hot: true,
         open: true,/* Abril el navegdor de manera automatica */
         port: 9000,/* con el puerto 9000 */
     },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',// Intercepta los archivos de js y transpila en versiones más antiguas que entiendan la mayoría de los navegadores
                exclude: /node_modules/, //Excluimos la carpeta de node_modules para no tener un problema de Performance
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                       loader: 'css-loader',
                       options: {
                           importLoaders: 1
                       }
                    },
                    'postcss-loader'
                ]
            },
            {
                test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 90000,
                    }
                },
            },
            {
				test: /\.scss$/,
				use: [ 

					"style-loader",
					"css-loader",
					"sass-loader"
					]
			},
			{
				test: /\.less$/,
				use: [ 

					"style-loader",
					"css-loader",
					"less-loader"
					]
			},
			{
				test: /\.styl$/,
				use: [ 

					"style-loader",
					"css-loader",
					"stylus-loader"
					]
			}
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWepackPlugin({
            title: 'webpack-dev-server',
            template: path.resolve(__dirname, 'index.html')
        })
    ],
    optimization: {
        splitChunks: {
            chunks: "all",
            minSize: 0,
            name: "commons"
        }
    }
}

