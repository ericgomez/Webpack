import React from 'react'

function Loader(props) {
    return (
        <li>
            {
                //props.data.name
                props.name
            }
        </li>
    )
}

// Otra manera de obtener solo el nombre

// function Loader({ name }) {
//     return (
//         <li>
//             {
//                 name
//             }
//         </li>
//     )
// }

export default Loader