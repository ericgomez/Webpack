import React, {useState} from 'react'
import data from './data.json'
import Loader from './loader'
import logo from '../../images/platzi.png'
import video from '../../video/que-es-core.mp4'

console.log(data);

function App() {
    const [loaderList, setLoaderList] = useState([])//Lista de Loaders

    function handleClick() {
        setLoaderList(data.loaders)
    }

    return (
        <div>
            Mi primer aplicacion hecha en React.js
            <video src={ video } width={ 360 } controls poster={ logo }></video>
            <p> 
                <img src={ logo } alt="" width={ 40 }/>
            </p>
        <ul>
            {
                //loaderList.map(item => <Loader data= {item} key= {item.id}/>)
                loaderList.map(item => <Loader {...item} key= {item.id}/>)
            }
        </ul>
        <button onClick={ handleClick } >Mostrar los aprendido hasta el momento</button>
        </div>
    )
}

export default App
    